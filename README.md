**Tâches à faire (Sur Symfony 5) :**

_Front (responsive) :_
- Créer une Home comme sur la maquette
- Créer une page "On recrute"
-   Pouvoir lister des annonces de recrutement
- Formulaire de contact

_Backoffice :_
- Pas besoin de login pour ce test
- Lister les types de poste (Technicien, Cadre, Manutention, ...)
- Pouvoir ajouter des types de poste
- Lister les annonces de recrutement
- Pouvoir ajouter une annonce avec les champs :
- -Type du poste (via une liste dynamique)
- -Nom de l'annonce
- -Description de l'annonce
- -Pouvoir desactiver une annonce

Ne pas oublier de joindre la BDD au dépôt.
